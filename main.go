package main

import (
	arqClient "dcp/client"
	arqServer "dcp/server"
	u "dcp/utils"
	"flag"
	"os"
)

// command arguments
var (
	serverMode  = flag.Bool("server", false, "Run as server")
	destination = flag.String(
		"destination",
		"0.0.0.0:7227",
		"Destination/listen host:port",
	)
	filePath = flag.String("file", "", "File to send")
)

func main() {
	flag.Parse()

	if *serverMode {
		server := arqServer.NewArqServer(*destination)
		server.Run()
	} else {
		file, err := os.Open(*filePath)
		u.CheckError(err)
		defer file.Close()

		client := arqClient.NewArqClient(*destination)
		conn := client.Handshake()

		client.Send(conn, file)
	}
}
