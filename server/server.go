package server

import (
	"dcp/packet"
	u "dcp/utils"
	"fmt"
	"log"
	"net"
	"os"
)

type ArqServer struct {
	Algorithm            string
	Destination          string
	UdpAddr              *net.UDPAddr
	ActiveConnectionAddr *net.UDPAddr
	HasActiveConnection  bool
	HasFileMeta          bool
	FileMeta             packet.FileMeta
	OpenedFile           *os.File
	RN                   uint
}

func NewArqServer(destination string) *ArqServer {
	udpAddr, err := net.ResolveUDPAddr("udp", destination)
	u.CheckError(err)

	server := ArqServer{
		Destination:         destination,
		Algorithm:           "SW",
		UdpAddr:             udpAddr,
		HasActiveConnection: false,
		HasFileMeta:         false,
	}

	return &server
}

func (self *ArqServer) Run() {
	fmt.Printf("start serving: %s\n", self.Destination)

	conn, err := net.ListenUDP("udp", self.UdpAddr)
	u.CheckError(err)

	for {
		self.HandleConnection(conn)
	}
}

func (self *ArqServer) SetActiveConnection(conn *net.UDPConn, addr *net.UDPAddr) {
	self.ActiveConnectionAddr = addr
	self.HasActiveConnection = true
}

func (self *ArqServer) Reset(conn *net.UDPConn) {
	self.RN = 0
	self.UnsetActiveConnection(conn)
	self.UnsetFileMeta()
}

func (self *ArqServer) SetFileMeta(recvBuf []byte, conn *net.UDPConn) error {
	var fileMeta packet.FileMeta

	err := u.Unmarshal(recvBuf, &fileMeta)
	if err != nil {
		return err
	}

	self.FileMeta = fileMeta
	self.HasFileMeta = true

	file, err := os.Create(self.FileMeta.Name)
	if err != nil {
		self.Reset(conn)
		return err
	}
	self.OpenedFile = file

	return nil
}

func (self *ArqServer) UnsetFileMeta() {
	self.HasFileMeta = false
	self.FileMeta = packet.FileMeta{}
}

func (self *ArqServer) UnsetActiveConnection(conn *net.UDPConn) {
	self.HasActiveConnection = false
	self.ActiveConnectionAddr = nil
}

func (self *ArqServer) Handshake(recvBuf []byte, conn *net.UDPConn, addr *net.UDPAddr) error {
	var handshake packet.Handshake

	err := u.Unmarshal(recvBuf, &handshake)
	if err != nil {
		return err
	}

	handshake.ACK = handshake.SYN + 1
	encoded, err := u.Marshal(handshake)

	_, err = conn.WriteToUDP(encoded, addr)
	if err != nil {
		return err
	}

	return nil
}

func (self *ArqServer) SaveData(data []byte) error {
	_, err := self.OpenedFile.Write(data)

	return err
}

func (self *ArqServer) HandleConnection(conn *net.UDPConn) {
	var recvBuf = make([]byte, 98304)
	var decodedData packet.Data

	// Read data from socket
	_, addr, err := conn.ReadFromUDP(recvBuf)
	if err != nil {
		return
	}

	// Check if exist active connection
	if self.HasActiveConnection == false {
		err = self.Handshake(recvBuf, conn, addr)

		if err == nil {
			self.SetActiveConnection(conn, addr)

		}
		return
	}

	// Check if we have metadata for file
	if self.HasFileMeta == false {
		err = self.SetFileMeta(recvBuf, conn)
		if err != nil {
			log.Println(err.Error())
		}
		return
	}

	// Decode data packet
	err = u.Unmarshal(recvBuf, &decodedData)
	if err != nil {
		log.Println(err.Error())
		return
	}

	// Check final packet
	if decodedData.Final {
		self.OpenedFile.Close()
		self.Reset(conn)
		return
	}

	// Confirm packet
	var encodedData []byte
	if decodedData.RN == self.RN {
		decodedData.Confirm()
		self.RN = decodedData.RN

	} else {
		log.Printf("RN not in line local  %d, remote %d\n",
			self.RN,
			decodedData.RN,
		)
		return
	}

	// Write data to file
	err = self.SaveData(decodedData.Data)
	if err != nil {
		log.Println(err.Error())
		return
	}

	// Write answer
	decodedData.Data = []byte("")
	encodedData, err = u.Marshal(decodedData)
	if err != nil {
		return
	}
	_, err = conn.WriteToUDP(encodedData, addr)
	if err != nil {
		fmt.Printf("When write to %v error occured: %s\n", addr, err.Error())
		return
	}

}
