package client

import (
	"dcp/packet"
	u "dcp/utils"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"time"
)

// ArqClient represent client for fast transmitting data
type ArqClient struct {
	Algorithm   string
	Destination string
	UdpAddr     *net.UDPAddr
}

// NewArqClient initialize new ArqClient
func NewArqClient(destination string) *ArqClient {
	udpAddr, err := net.ResolveUDPAddr("udp", destination)
	u.CheckError(err)

	client := ArqClient{
		Algorithm:   "SW",
		Destination: destination,
		UdpAddr:     udpAddr,
	}
	return &client
}

// Handshake with server
func (self *ArqClient) Handshake() *net.UDPConn {
	var recvBuf = make([]byte, 2048)
	conn, err := net.DialUDP("udp", nil, self.UdpAddr)
	u.CheckError(err)
	conn.SetReadDeadline(time.Now().Add(5 * time.Second))

	data := packet.Handshake{ACK: 0, SYN: 1}

	encoded, err := u.Marshal(data)
	u.CheckError(err)

	_, err = conn.Write(encoded)
	u.CheckError(err)

	_, err = conn.Read(recvBuf[0:])
	u.CheckError(err)

	var decoded packet.Handshake
	err = u.Unmarshal(recvBuf, &decoded)
	u.CheckError(err)

	return conn
}

func (self *ArqClient) SendFileMeta(conn *net.UDPConn, file *os.File) error {
	info, err := file.Stat()
	if err != nil {
		return err
	}

	var fileMeta = packet.FileMeta{
		Username: "GoldenFish",
		Password: "crackedHash",
		Name:     "test.test",
		Size:     info.Size(),
	}

	encoded, err := u.Marshal(fileMeta)
	if err != nil {
		return err
	}

	_, err = conn.Write(encoded)

	return err
}

func (self *ArqClient) ShowInfo(conn *net.UDPConn, file *os.File) {
	fileInfo, err := file.Stat()
	if err != nil {
		panic(err.Error())
	}

	fmt.Printf("Client started at %s with params:\n",
		time.Now().Format(time.RFC3339))
	fmt.Printf("Clien mode is: True\n")

	fmt.Printf("Destination is: %s\n", conn.RemoteAddr())
	fmt.Printf("Windowsize is: %d\n", 0)
	fmt.Printf("Payload size is: %d bytes\n", fileInfo.Size())
	fmt.Printf("Making a log to : Stdout\n")
}

// Send read data from file and write to udp connection
func (self *ArqClient) Send(conn *net.UDPConn, file *os.File) {
	var sendBuf = make([]byte, 1024)
	var recvBuf = make([]byte, 1024)

	var sendedFileBytes, writedToConnectionTotal uint
	var timeout time.Duration = 5
	var retry = 100
	var n = 0
	var RN uint = 0

	self.ShowInfo(conn, file)
	err := self.SendFileMeta(conn, file)
	u.CheckError(err)

	for {
		readedFromFile, err := file.Read(sendBuf)
		if err == io.EOF {
			log.Printf("End of file. Size %d\n", sendedFileBytes)
			data := packet.Data{RN: RN, Data: []byte(""), Final: true}
			encoded, err := u.Marshal(data)
			u.CheckError(err)
			_, err = conn.Write(encoded)

			break
		}
		//fmt.Printf("Readed from file %d\nString what readed: %s\n", readedFromFile, string(sendBuf[:readedFromFile]))
		sendedFileBytes += uint(readedFromFile)

		data := packet.Data{RN: RN, Data: sendBuf[:readedFromFile]}
		encoded, err := u.Marshal(data)
		u.CheckError(err)

		n = retry
		for ; n > 0; n-- {
			conn.SetReadDeadline(time.Now().Add(timeout * time.Second))
			writedToConnection, err := conn.Write(encoded)

			readedFromConn, err := conn.Read(recvBuf)
			if err != nil {
				fmt.Printf("error %T\n", err)
				switch err := err.(type) {
				case net.Error:
					if err.Timeout() {
						log.Println("This was a net.Error with a Timeout")
						log.Printf("Retry %d\n", n)
						continue
					}
					if err.Temporary() {
						log.Println("This was a net.Error with a Temporary")
						log.Printf("Retry %d\n", n)
						continue
					}
				}
				u.CheckError(err)
			}

			var answer packet.Data
			err = u.Unmarshal(recvBuf[:readedFromConn], &answer)
			if err != nil {
				log.Printf("When unmarshalling answer %v error occured: %s",
					conn.RemoteAddr(), err.Error())
				continue
			}
			log.Println("Packet RN ", answer.RN)
			if answer.RN == RN+1 {
				RN += 1
				writedToConnectionTotal += uint(writedToConnection)
				//fmt.Printf("Packet received: RN - %d\nReaded from connection: %d\n", RN, readedFromConn)
			} else {
				continue
			}

			//break if transmit packet
			break
		}
		if n <= 0 {
			panic("cannot send file")
		}
	}

	fmt.Printf("File '%s' sended.\nTotal sended bytes: %d\n",
		file.Name(),
		writedToConnectionTotal,
	)

}
