package interfaces

type IArqServer interface {
	Run()
}

type IArqClient interface {
	Send()
	Handshake()
}
