package packet

// Handshake represent handshake structure with the server
type Handshake struct {
	SYN int
	ACK int
}
