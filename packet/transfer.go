package packet

// Meta represent file metadata
// And userdata
type FileMeta struct {
	Username string
	Password string
	Name     string
	Size     int64
}

type ControlMessage struct {
	Code string
	Msg  string
}

// Data represents data structure to send
type Data struct {
	RN    uint
	Data  []byte
	Final bool
}

func (self *Data) Confirm() {
	self.RN += 1
}
